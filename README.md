# StageVerslagen

## 2018-10-01 (Dag 1)

In de voormiddag hebben we een desktop gebouwd. Vervolgens hebben we met Rufus het iso-bestand van Windows Server op een USB-stick gezet en Windows Server geïnstalleerd op de desktop.
We hebben ook meegekeken bij de vervanging van een iPad-scherm en we hebben een server die geconfigureerd werd voor een bedrijf in Herentals (eerste helft) en het andere deel van de server voor een datacenter ergens anders (tweede helft) mogen observeren.
In de namiddag hebben we vooral gewerkt aan taken. Hierbij hebben we het assortiment van de winkel onderzocht. Ook hebben we het toetsenbord uit een laptop gehaald en enkele foto's genomen van de binnenkant van een laptop.

## 2018-10-02 (Dag 2)
We zijn de dag gestart met het onderzoeken van een defecte printer. Deze had een kapotte printkop, de inkt had deze verstopt en het is op deze manier verbrand. Het viel dus niet te repareren. Ook hebben we een defecte laptop geanalyseerd. Deze deed niks meer en na analyse bleek dat het moederbord defect was (liet geen stroom meer door op elke plek).
Na de middag hebben we op onze Windows Server AD en DNS geïnstalleerd en deze naar een DC gepromoveerd. Daarna hebben we DHCP geïnstalleerd en een laptop aan het netwerk toegevoegd. Deze hebben we een user toegewezen en deze aan ons domein stage.local toegevoegd. Tussen al deze gebeurtenissen hebben we ook nog aan taken gewerkt.
Tenslotte hebben we nog een hardware-test gedaan op een defecte iMac.

## 2018-10-03 (Dag 3)
We zijn in de voormiddag verder gegaan met ons project. We hebben de clients toegevoegd aan het netwerk en beide een user gegeven. Daarna hebben we op één van de twee laptops een webserver geïnstalleerd.
In de namiddag hebben we onze case study gemaakt over ons netwerk met een webserver (interne website door IIS). We zijn later in de namiddag foto's gaan nemen in het bedrijf voor onze Franse GIP-taak.
Tenslotte hebben we nog meegekeken bij de vervanging van een laptopbatterij en de reparatie van een iMac.

## 2018-10-04 & 2018-10-05 (Dag 4 & 5)
Het was erg rustig de laatste twee dagen. We hebben ons nog verder bezig gehouden met de iMac. We hebben ook nog aan taken gewerkt. Ten slotte hebben we toetsenborden getest en ze gerepareerd.